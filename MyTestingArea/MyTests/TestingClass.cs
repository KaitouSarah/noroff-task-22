﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calculator;
using Xunit;

namespace MyTests
{
    public class TestingClass
    {
        CalculatorProgram calculator = new CalculatorProgram();

        [Fact]
        public void Add_AddingCorrecly()
        {
            double expected = 42;

            double actual = calculator.Add(22, 20);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Subtract_SubtractingCorrectly()
        {
            double expected = 42;

            double actual = calculator.Subtract(42.5, 0.5);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Multiply_MultiplyingCorreclty()
        {
            double expected = 42;

            double actual = calculator.Multiply(5.25, 8);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Divide_DividingCorrectly()
        {
            double expected = 42;

            double actual = calculator.Divide(126, 3);

            Assert.Equal(expected, actual);
        }

        //Checks that the method throws excetion when trying
        //to devide by zero
        [Fact]
        public void Divide_ExceptionWhenDividingByZero()
        {
            Action action = () => calculator.Divide(42, 0);

            Assert.Throws<ArgumentException>(action);
        }

        //Checks that the method throws exception when trying 
        //to add two numbers that combined are larger than what 
        //a double can be
        [Fact]
        public void Add_ExceptionWhenResultTooLarge()
        {
            Action action = () => calculator.Add(Double.MaxValue, 1);

            Assert.Throws<ArgumentOutOfRangeException>(action);

        }
    }
}
