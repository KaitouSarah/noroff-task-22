﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    public class CalculatorProgram
    {
        public CalculatorProgram() { }

        public double Add(double x, double y)
        {
            //If the sum of x and y minus 1 is equal (or bigger) than the amx value of a double,
            //the sum cannot be calculated.
            if (x + y - 1 >= Double.MaxValue)
            {
                throw new ArgumentOutOfRangeException();
            }

            return x + y;
        }

        public double Subtract(double x, double y)
        {
            return x - y;
        }

        public double Multiply(double x, double y)
        {
            return x * y;
        }

        public double Divide(double x, double y)
        {
            if (y == 0)
            {
                throw new ArgumentException();
            }

            return x / y;
        }
    }
}
