# Noroff Task 22

Part 2 - Task 22

� Create a Winformsolution call MyTestingArea 

� Add a project (Class Library) called myTests 

� Write tests for the four methods that will be used in a basic calculator application (Add, Subtract, Multiply, Delete) 

� Then add another project (Class library). Write the four methods to satisfy the tests 

� Think about situations where your methods will cause errors and write at least two tests for two possible failures (any of the methods). You can do one for one method and one for another method